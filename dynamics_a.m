function dadt = dynamics_a(t, a, xi_pr_squar, epsilon)

dadt = -epsilon * a * xi_pr_squar;
end
